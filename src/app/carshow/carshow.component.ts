import { Component, OnInit } from '@angular/core';
import { map, filter, retry, catchError } from 'rxjs/operators';
import { CarshowsService } from '../carshows.service';

@Component({
  selector: 'app-carshow',
  templateUrl: './carshow.component.html',
  styleUrls: ['./carshow.component.scss']
})
export class CarshowComponent implements OnInit {

  public carShowList;

  constructor(private data: CarshowsService) { }

  ngOnInit() {
    this.data.getCarShows().subscribe(data => {
      this.carShowList = data;
    })
  }

}
