import { TestBed } from '@angular/core/testing';

import { CarshowsService } from './carshows.service';

describe('CarshowsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CarshowsService = TestBed.get(CarshowsService);
    expect(service).toBeTruthy();
  });
});
