import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})

export class CarshowsService {

  constructor(private http: HttpClient) { }

  getCarShows() {
    const httpHeaders = new HttpHeaders ({
      'Content-Type': 'application/json'
    });
    return this.http.get(`http://eacodingtest.digital.energyaustralia.com.au/api/v1/cars`, { headers: httpHeaders })
  }

}
